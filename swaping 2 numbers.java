//swaping 2 numbers


		        int num1 = 5; // First number
		        int num2 = 10; // Second number
		        
		        System.out.println("Before swapping:");
		        System.out.println("num1 = " + num1);
		        System.out.println("num2 = " + num2);
	        
		        // Swapping logic using a temporary variable
		        int temp = num1;
		        num1 = num2;
		        num2 = temp;
		        
		        System.out.println("\nAfter swapping:");
		        System.out.println("num1 = " + num1);
		        System.out.println("num2 = " + num2);
