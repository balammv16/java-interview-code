
public class looping 
{
	public static void main(String[] args) 
    {
        	 int limit = 100;                                    // Specify the limit
		        System.out.println("Numbers divisible by both 3 and 5 up to " + limit + ":");
		        for (int i = 1; i <= limit; i++) 
                {
		            if (i % 3 == 0 && i % 5 == 0)   
                    {                                           // Check if divisible by both 3 and 5
		                System.out.println(i);                  // Print the number
		            }
		        }
	}
}
Numbers divisible by both 3 and 5 up to 100:
15
30
45
60
75
90

