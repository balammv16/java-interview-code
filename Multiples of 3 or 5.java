public class looping 
{
	public static void main(String[] args) 
    {
        	 int limit = 100;                                    // Specify the limit
		        System.out.println("Numbers divisible by both 3 or 5 up to " + limit + ":");
		        for (int i = 1; i <= limit; i++) 
                {
		            if (i % 3 == 0 || i % 5 == 0)   
                    {                                           // Check if divisible by both 3 and 5
		                System.out.println(i);                  // Print the number
		            }
		        }
	}
}

Numbers divisible by both 3 or 5 up to 100:
3 5 6 9 10 12 15 18 20 21 24 25 27 30 33 35 36 39 40 42 45 48 50 51 54 55 57 60 63 65 66 69 70 72 75 78 80 81 84 85 87 90 93 95 96 99 100 
